function output = fft_calc(data_mat, baseline, cutoff_val)
% param data_mat: first column is frequency (Hz), second column is
% magnitude. This can be extracted from the FFT screen of an oscilloscope
% using ScopeGrab
% param baseline: same as data_mat but with no signal. This is the
% baseline noise
% param cutoff_val: fundamental freq you wish to examine

% set missing params
if nargin < 2
    baseline = 0;
    cutoff_val = 50000;
end

if nargin < 3
    cutoff_val = 50000;
end

% extract necessary values
data = data_mat(:,2);
xvals = data_mat(:,1);
min_data = min(data);

% Normalize 0 to minimum data value
data = data - min_data;

% Select 5 kHz as bandwidth cutoff
cutoff = 5000;

% iterate over data to identify data within bandwidth
a = 1;
for n = 1:length(xvals)
    if xvals(n) < (cutoff_val + cutoff) && xvals(n) > (cutoff_val - cutoff)
        cutoff_xvals(a) = xvals(n);
        data_xvals(a) = data(n);
        a=a+1;
    end
end

% process baseine
baseline_x = baseline(:,1);
baseline_dat = baseline(:,2);
min_base = min(baseline_dat);
baseline_dat = baseline_dat - min_base;

% integrate to find total energy and energy within bandwidth
total_energy = trapz(xvals, data) - trapz(baseline_x, baseline_dat);
cutoff_energy = trapz(cutoff_xvals, data_xvals);

% calculate percentages
percent_in_cutoff = cutoff_energy/total_energy;

output = percent_in_cutoff;